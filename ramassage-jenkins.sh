#!/bin/bash

readonly delivery_dir="delivery"
export delivery_dir

function errcho(){ >&2 echo "$@"; }
export -f errcho
function term_newline(){ if [ -t 0 ]; then echo; fi }

function write_to_csv()
{
    local data=("$@")
    local NOTES_CSV="notes.csv"
    local DETAILS_CSV="details.csv"
    local notes=""
    local details=""
    local expected_len=5
    local len=${#data[@]}
    
    if [ $len -ne $expected_len ]; then # Protection for devs, check that every line contains all informations
	errcho "DEV ERROR: Bad array size of $len instead of $expected_len in write_to_csv."
	exit 2
    fi
    
    for (( i=0; i < len; i++ )); do
	if [ $i -lt 3 ]; then
	    notes=$notes${data[$i]}
	    if [ $i -lt 2 ]; then
		notes=$notes';'
	    fi
	fi
	details=$details${data[$i]}
	if [ $((i + 1)) -lt $len ]; then
	    details=$details';'
	fi
    done

    echo "$notes" >> "$NOTES_CSV"
    echo "$details" >> "$DETAILS_CSV"
}
export -f write_to_csv

function connect_to_jenkins()
{
    local url=$1
    local login=$2
    local password=$3

    echo -n "Trying to connect to Jenkins at $url with user $login"
    curl -fs "$url" -u "$login:$password" > /dev/null 2> /dev/null
    if [ $? -ne 0 ]; then
	echo -e "\033[31;1m KO\033[0m"
	errcho "> Check your connection and your credentials";
	exit 1
    fi
    echo -e "\033[32;1m OK\033[0m"
    return 0
}

function try_project_url()
{
    local url=$1
    local login=$2
    local password=$3

    echo -n "Project url: $url"
    curl -fs "$url" -u "$login:$password" > /dev/null 2> /dev/null
    if [ $? -ne 0 ]; then
	echo -e "\033[31;1m KO\033[0m"
	return 1
    fi
    echo -e "\033[32;1m OK\033[0m"
    return 0
}

function determine_project_url()
{
    local  __resultvar=$1
    local jk_url=$2
    local login=$3
    local password=$4
    local module=$5
    local project=$6
    local year=$7

    # Try with latest jenkins hierarchy first
    local project_url="$jk_url/job/$module/job/$project/job/$year/"
    try_project_url "$project_url" "$login" "$password"
    if [ $? -eq 0 ]; then
	eval $__resultvar="'$project_url'"
	return 0
    fi
    return 1
}

function get_student_project()
{
    local url=$1
    local jk_login=$2
    local jk_password=$3
    local student_login=$4

    if [ "$result_only" -eq 1 ]; then
	local files="norm.dpr,norm.note,note.txt,tests_report.xml,trace.txt"
	curl -fs "$url/ws/{$files}" -u "$jk_login:$jk_password" -o "$student_login/#1" > /dev/null 2> /dev/null
	if [ $? -ne 0 ]; then
	    return 1
	fi
	curl -fs "$url/ws/$delivery_dir" -u "$jk_login:$jk_password" > /dev/null 2> /dev/null
	if [ $? -eq 0 ]; then
	    mkdir -p "$student_login/$delivery_dir"
	    echo "You can see the delivery at $url/ws/$delivery_dir" > "$student_login/$delivery_dir/THERE_IS_SOMETHING"
	fi
    else
	curl -s "$url/ws/*zip*/$student_login.zip" -u "$jk_login:$jk_password" -o "$student_login.zip" > /dev/null 2> /dev/null
	unzip -q "${student_login}.zip" > /dev/null 2> /dev/null
	if [ $? -ne 0 ]; then
	    rm -f "$student_login.zip"
	    return 1
	fi
    fi
    curl -s "$url/lastBuild/consoleText" -u "$jk_login:$jk_password" -o "$student_login/console.log" > /dev/null 2> /dev/null
    rm -rf "$student_login.zip" "$student_login"/Always-*.txt  "$student_login/mouli" "$student_login/$delivery/.git"
    return 0    
}
export -f get_student_project

function generate_comment()
{
    local note_norm=$1
    local prereq=$2

    if [ "$nonorm" -eq 1 ]; then
	note_norm=0
    fi
    local comment=$(grep "^[^>;]" "trace.txt")

    if [ "$prereq" -ne 0 -a "$note_norm" -lt 0 ]; then
	comment=$comment$'\n'"Norm: $note_norm"
    elif [ "$note_norm" -eq 1 ]; then
	comment='Too much norm errors.'
    fi
    echo -n "$comment"
}
export -f generate_comment

function calc_note()
{
    local note_norm=$1
    if [ "$nonorm" -eq 1 ]; then
	note_norm=0
    fi
    local tests=$2
    local note=$(echo "scale=2;$tests / 100 * 18 + 2 + $note_norm" | bc)
    note=$(echo "scale=1;$note" | bc)
    echo -n "$note"
}
export -f calc_note

function calc_test_percentage()
{
    local passed_tests
    local total_tests

    if [ "$bypass_dl" -eq 1 ]; then
	passed_tests=$(grep "^[^>;]" "trace.txt" | grep -Eo '[[:digit:]]+/[[:digit:]]+' | cut -d '/' -f1 | sed 's/$/+/' | tr -d $'\n' | sed $'s/$/0\\n/' | bc)
	total_tests=$(grep "^[^>;]" "trace.txt" | grep -Eo '[[:digit:]]+/[[:digit:]]+' | cut -d '/' -f2 | sed 's/$/+/' | tr -d $'\n' | sed $'s/$/0\\n/' | bc)
	if [ ! "$passed_tests" ] || [ ! "$total_tests" ]; then
	    passed_tests=0
	    total_tests=1
	fi
    else
	local apiResult=$(curl "$student_project_url/lastBuild/testReport/api/xml?tree=passCount,failCount" -u "$jk_login:$jk_passwd" -s 2> /dev/null)
	local failed_tests=$(echo "$apiResult" | sed 's/.*<failCount>\(.*\)<\/failCount>.*/\1/')
	passed_tests=$(echo "$apiResult" | sed 's/.*<passCount>\(.*\)<\/passCount>.*/\1/')
	# local failed_tests=$(echo "$apiResult" | xpath '//failCount[1]/text()' 2>/dev/null)
	# passed_tests=$(echo "$apiResult" | xpath '//passCount[1]/text()' 2>/dev/null)
	total_tests=$(echo "$failed_tests+$passed_tests" | bc)
    fi
    local tests=$(echo "scale=2;$passed_tests/$total_tests" | bc)
    tests=$(echo "$tests*100" | bc)
    tests=${tests%.*}
    echo "$tests"
}
export -f calc_test_percentage

function calc_student_result()
{
    local student_login=$1

    local note=2
    local note_norm=$(cat "norm.note")
    local tests=$(calc_test_percentage)
    diff 'note.txt' 'trace.txt' 2> /dev/null >/dev/null
    local prereq=$?
    grep -E '(Crashed|Execution failed)' 'trace.txt' > /dev/null
    local did_not_crash=$?
    if [ \( "$nonorm" -eq 0 -a "$note_norm" -eq 1 \) -o $prereq -eq 0 ]; then
    	note='1'
    elif [ $did_not_crash -eq 0 ]; then
    	note='1.5'
    elif [[ -z $tests || $tests -eq 0 ]]; then
    	tests='0'
    	note='1'
    else
    	note=$(calc_note "$note_norm" "$tests")
    	if [ "$(echo "$note<2" | bc)" -eq 1 ] # Don't go below 2 because of the norm
    	then
    	    note=2
    	fi
    fi
    local comment=$(generate_comment "$note_norm" "$prereq")
    data=("$student_login" "$note" "\"$comment\"" "$note_norm" "$tests")
}
export -f calc_student_result

function is_project_turn_in()
{
    if [[ -d "$delivery_dir" && -n "$(ls "$delivery_dir")" ]]; then
	return 0
    fi
    return 1
}
export -f is_project_turn_in

function write_for_group()
{
    local logins=$(sed '/^BUGS_LOGIN=\([^ ]*\)$/!d;s//\1/' "$student_login/console.log" 2>/dev/null | tr ',' ' ')
    echo "$logins" > "$student_login/group"
    if [ ! "$logins" ] || [ "$logins" = "$student_login" ]; then
	logins="$student_login"
    else
	errcho -n '  Detected group: '
	errcho "$logins"
	logins=$(echo "$logins" | tr ' ' '\n')
    fi
    for login in $logins; do
	data[0]="$login"
	write_to_csv "${data[@]}"
    done
}
export -f write_for_group

function process_student_project()
{
    local student_project_url=$1
    local jk_login=$2
    local jk_passwd=$3
    local student_login=$4

    local data=("$student_login" 0 "\"[INTRA] Not registered\"" 0 0)    

    echo -n "# Processing $student_login"

    local got_project=1
    mkdir -p "$student_login"
    if [ "$bypass_dl" -eq 0 ]; then
    	get_student_project "$student_project_url" "$jk_login" "$jk_passwd" "$student_login"
	if [ $? -ne 0 ]; then
	    got_project=0
	fi
    fi
    pushd "$student_login" > /dev/null
    if [ "$got_project" -eq 1 ]; then
    	is_project_turn_in
    	if [ $? -eq 0 ]; then
    	    calc_student_result "$student_login"
    	else
    	    data[1]="0.5"
    	    data[2]='"Nothing turn in."'
    	fi
    	echo -e "\033[32;1m OK\033[0m"
    else
    	echo -e "\033[93;1m CHECK MANUALLY\033[0m"
    	errcho "> $student_login not found on jenkins for this project. He may not be registered to it or the last build may have failed."
    fi
    popd > /dev/null
    write_for_group
    if [ "$csv_only" -eq 1 ]; then
    	rm -rf "$student_login"
    fi
}
export -f process_student_project


function launch_ramassage()
{
    local instance="$1"
    local project_dir="${jk_project}_${jk_year}_${instance}"
    local instance_project_url="$jk_project_url/job/$instance/"

    echo "Launching project collection for $instance"

    if [ "$bypass_dl" -eq 0 ]; then
	# Clean up and create directory for the project
	rm -rf "$project_dir" ; mkdir "$project_dir"
    fi

    pushd "$project_dir" > /dev/null

    # Write the header of the csv files
    rm -f 'notes.csv' 'details.csv'
    local header=("login" "note" "comment" "norm" "tests")
    write_to_csv "${header[@]}"

    if [ "$auto_logins" -eq 1 ] # If we don't have login, we'll get it from the Jenkins project page
    then
	if [ "$bypass_dl" -eq 0 ]; then
	    curl "$instance_project_url" -u "$jk_login:$jk_passwd" -s | sed -n 's/.*\/\(.*\)\/lastBuild.*/\1/p' > logins
	else
	    rm -f logins
	    for i in $(ls -d */); do echo ${i%%/} >> logins; done
	fi
	logins="logins"
    fi
 
    if [ "$nonorm" -eq 1 ]; then
	errcho "> Norm not checked"
    fi

    # Run the handling either with 'parallel' or sequentially
    hash parallel 2>/dev/null
    if [ $? -ne 0 ]; then
	errcho "> Process students sequentially (SLOW), consider installing 'parallel'"
	while read student_login ; do
	    process_student_project "$instance_project_url/job/$student_login" "$jk_login" "$jk_passwd" "$student_login"
	done < "$logins"
    else
	errcho "> Use 'parallel' to process students"
	parallel -j8 -k process_student_project "$instance_project_url/job/{}" "$jk_login" "$jk_passwd" {} < "$logins"
    fi

    rm -f logins

    popd > /dev/null

    if [ "$compress" -eq 1 ]; then
	rm -f "${project_dir}.zip"
	zip -r "${project_dir}.zip" "${project_dir}"
    fi
    if [ "$cleanup" -eq 1 ]; then
	rm -rf "$project_dir"
    fi
}

function fetch_instances()
{
    if [ "$bypass_dl" -eq 0 ]; then
	echo $(curl "$jk_project_url" -u "$jk_login:$jk_passwd" -s | sed -nE 's/.*\/([A-Z]{3}-[0-9]-[0-9])\/lastBuild.*/\1/p' | tr '\n' ' ')
    else
	for i in $(ls -d */); do
	    local instance=$(echo -n "${i%%/} " | tail -c8)
	    echo "$instance";
	done
    fi

}

#####################################
#               START               #
#####################################

nonorm=0
csv_only=0
compress=0
cleanup=0
auto_logins=1
bypass_dl=0
result_only=0
for param in "$@"
do
    if [ "x$param" = "x-nonorm" ]; then
	nonorm=1
    elif [ "x$param" = "x-csv_only" ]; then
	csv_only=1
    elif [ "x$param" = "x-compress" ]; then
	compress=1
    elif [ "x$param" = "x-cleanup" ]; then
	cleanup=1
    elif [ "x$param" = "x-bypass_dl" ]; then
	bypass_dl=1
    elif [ "x$param" = "x-result_only" ]; then
	result_only=1
    else
	logins=$(readlink -f "$param" 2> /dev/null)
	auto_logins=0
	if [  ! -f "$logins" ]; then
	    errcho "Missing file \"$param\"."
	    exit 1
	fi
    fi
done
export nonorm
export csv_only
export compress
export cleanup
export auto_logins
export bypass_dl
export result_only

jk_base_url='https://master.ci.epitech.eu'

# If in terminal mode, ask informations to user
read -p "Jenkins login: " jk_login
read -p "Password: " -s jk_passwd
term_newline
read -p "Module(ex: B-CPE-084): " jk_module
read -p "Project (ex: bsq): " jk_project
read -p "Year: " jk_year
read -p "Instance (ex: PRS-1-1): " jk_instance
term_newline


if [ "$bypass_dl" -eq 0 ]; then
    connect_to_jenkins "$jk_base_url" "$jk_login" "$jk_passwd"
    rm -rf "$jk_project"; mkdir "$jk_project";
fi
cd "$jk_project"

jk_instance=$(echo "$jk_instance" | tr ' ' '\n')

if [ "$bypass_dl" -eq 0 ]; then
    determine_project_url jk_project_url "$jk_base_url" "$jk_login" "$jk_passwd" "$jk_module" "$jk_project" "$jk_year"
    if [ $? -eq 1 ]; then
	# If we're here, we can't find the project url :(
	errcho "> Cannot find project url. Check your parameters."
	exit 1
    fi
    export jk_project_url
fi

if [ "$jk_instance" = "ALL" ]; then
    jk_instance=$(fetch_instances)
fi

for instance in $jk_instance; do
    launch_ramassage "$instance"
done
