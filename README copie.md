# README for ramassage-jenkins.sh

Ce script BASH parse un dossier sur jenkins et récupère les rendus des étudiants donnés dans un fichier. 
Il permet aussi de créer un .csv sous la forme "login;comment;note".
La note est calculée comme suit :
tests = couverture de test en %
norme = note de norme
prelim = 2

note = tests / 100 * 18 + norme + prelim
On obtiens ainsi une note sur 20 avec les préliminaires.

De plus, si le projet a crashé la note est mise à 1.5.
En cas d'absence de rendu la note est de 0.5
Si le pourcentage de tests passé est de 0, la note est mise à 1 (non fonctionnel/ébanche de code)
Si les tests obligatoire ne sont pas passé, la note est de 1
Si les préliminaires sont OK mais que la norme fait descendre la note en dessous de 2, celle-ci est remise à 2


Il est possible de spécifier plusieurs instances à récupérer en les séparants par un espace (ex: "STG-4-1 TLS-4-1").
Pour récupérer l'ensemble des instances d'un projet à une année donnée il suffit de spécifier "ALL" comme instance à récupérer.

utilisation : 
./ramassage-jenkins.sh [OPTIONS] [logins-file]

OPTIONS:
-nonorm: ne pas prendre la norme en compte pour la note (cas pour le C++ notamment)
-csv_only: garde uniquement les fichiers CSV et pas tous les rendus
-compress: compress le résultat en zip
-cleanup: supprime les dossiers contenant les résultats pour chaque instances traités
-bypass_dl: aucune interaction avec Jenkins (les rendus doivent être comme si le script les avait déjà téléchargés)
-result_only: ne récupère pas les rendus, seuls les fichiers nécessaires au script sont récupérés (accélère généralement le traitement)
